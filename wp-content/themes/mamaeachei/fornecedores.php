<?php 
  /*
   * Template Name: Fornecedores
   */ 
?>
<?php get_header(); ?>
<section class="innerContent">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2><?php the_title(); ?></h2>
      </div>
    </div>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">


      <h2 class="title_trace"><span>Categorias</span></h2>
      <?php 
        $args = array(
        'show_option_all'    => '',
        'orderby'            => 'name',
        'order'              => 'ASC',
        'style'              => 'list',
        'show_count'         => 0,
        'hide_empty'         => 0,
        'use_desc_for_title' => 1,
        'child_of'           => 0,
        'feed'               => '',
        'feed_type'          => '',
        'feed_image'         => '',
        'exclude'            => '',
        'exclude_tree'       => '',
        'include'            => '',
        'hierarchical'       => 1,
        'title_li'           => __( '' ),
        'show_option_none'   => __( '' ),
        'number'             => null,
        'echo'               => 1,
        'depth'              => 0,
        'current_category'   => 0,
        'pad_counts'         => 0,
        'taxonomy'           => 'fornecedores_categorias',
        'walker'             => null
        );
      ?>
      <ul class="custom_list">
        <?php wp_list_categories( $args ); ?>
      </ul>


      <h2 class="title_trace"><span>Por localização</span></h2>
      <?php 
        $args_estado = array(
        'show_option_all'    => '',
        'orderby'            => 'name',
        'order'              => 'ASC',
        'style'              => 'list',
        'show_count'         => 0,
        'hide_empty'         => 0,
        'use_desc_for_title' => 1,
        'child_of'           => 0,
        'feed'               => '',
        'feed_type'          => '',
        'feed_image'         => '',
        'exclude'            => '',
        'exclude_tree'       => '',
        'include'            => '',
        'hierarchical'       => 1,
        'title_li'           => __( '' ),
        'show_option_none'   => __( '' ),
        'number'             => null,
        'echo'               => 1,
        'depth'              => 0,
        'current_category'   => 0,
        'pad_counts'         => 0,
        'taxonomy'           => 'fornecedores_estados',
        'walker'             => null
        );
      ?>
      <ul class="custom_list">
        <?php wp_list_categories( $args_estado ); ?>
      </ul>

    </div>


    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 sidebar">
      <h2 class="title_trace"><span>Buscar Fornecedores</span></h2>
      <img src="<?php bloginfo('stylesheet_directory'); ?>/img/guia_de_fornecedores.jpg" alt="Guia de Fornecedores" class="img-responsive">
      <p>Esse espaço foi criado para que você, mamãe ou papai, tenha o conforto de buscar, em um só lugar, profissionais qualificados e recomendados por outros pais.</p>
      <form role="search" method="get" id="searchform" class="searchform" action="<?php echo site_url('/'); ?>">
        <p>Selecione a Categoria</p>
        <?php wp_dropdown_categories(array('name' => 'categoria', 'orderby' => 'name', 'order' => 'ASC', 'show_option_all'=>'Todas as Categorias','hide_empty' => 0, 'taxonomy' => 'fornecedores_categorias')); ?>
        <br>
        <p>Selecione o Estado</p>
        <?php wp_dropdown_categories(array('name' => 'estado', 'orderby' => 'name', 'order' => 'ASC', 'show_option_all'=>'Todas os Estados','hide_empty' => 0, 'taxonomy' => 'fornecedores_estados')); ?>
        <br>
        Pesquise por nome
        <input type="text" name="s" id="s" placeholder="Nome do Fornecedor..." class="form-control">
        <input type="hidden" name="post_type" value="guia_de_fornecedores" />
        <input type="submit" id="searchsubmit" value="Pesquisar" class="search">
      </form>
      
    </div>
    <?php endwhile; else : ?>
    <p>
      <?php _e( 'Desculpe, não encontramos nada.' ); ?>
    </p>
    <?php endif; ?>
  </div>
</section>
<?php get_footer(); ?>