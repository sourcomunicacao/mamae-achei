<?php // custom functions.php template @ digwp.com

// Widgets Area
function widgets_novos_widgets_init(){
	register_sidebar(
  	array(
    	'name' => 'Header',
      'id' => 'header_widgets',
      'before_widget' => '<div>',
      'after_widget' => '</div>',
      'before_title' => '<h2>',
      'after_title' => '</h2>', 
    )
  );
}
add_action('widgets_init', 'widgets_novos_widgets_init');

// add feed links to header
if (function_exists('automatic_feed_links')) {
	automatic_feed_links();
} else {
	return;
}
require_once('wp_bootstrap_navwalker.php');
add_action( 'after_setup_theme', 'wpt_setup' );
    if ( ! function_exists( 'wpt_setup' ) ):
        function wpt_setup() {  
            register_nav_menu( 'primary', __( 'Primary navigation', 'wptuts' ) );
        } endif;
		
//Gets post cat slug and looks for single-[cat slug].php and applies it
add_filter('single_template', create_function(
	'$the_template',
	'foreach( (array) get_the_category() as $cat ) {
		if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") )
		return TEMPLATEPATH . "/single-{$cat->slug}.php"; }
	return $the_template;' )
);

function new_subcategory_hierarchy() { 
    $category = get_queried_object();

    $parent_id = $category->category_parent;

    $templates = array();

    if ( $parent_id == 0 ) {
        // Use default values from get_category_template()
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";
        $templates[] = 'category.php';     
    } else {
        // Create replacement $templates array
        $parent = get_category( $parent_id );

        // Current first
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";

        // Parent second
        $templates[] = "category-{$parent->slug}.php";
        $templates[] = "category-{$parent->term_id}.php";
        $templates[] = 'category.php'; 
    }
    return locate_template( $templates );
}
add_filter( 'category_template', 'new_subcategory_hierarchy' );


// smart jquery inclusion
if (!is_admin()) {
	wp_deregister_script('jquery');
	wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false, '1.3.2');
	wp_enqueue_script('jquery');
}


// enable threaded comments
function enable_threaded_comments(){
	if (!is_admin()) {
		if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1))
			wp_enqueue_script('comment-reply');
		}
}
add_action('get_header', 'enable_threaded_comments');


// remove junk from head
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);





// custom excerpt length
function custom_excerpt_length($length) {
	return 20;
}
add_filter('excerpt_length', 'custom_excerpt_length');


// custom excerpt ellipses for 2.9+
function custom_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'custom_excerpt_more');

/* custom excerpt ellipses for 2.8-
function custom_excerpt_more($excerpt) {
	return str_replace('[...]', '...', $excerpt);
}
add_filter('wp_trim_excerpt', 'custom_excerpt_more'); 
*/


function add_resumo_com_link($more) {
       global $post;
	return '<a href="'. get_permalink($post->ID) . '">... Leia mais +</a>';
}
add_filter('excerpt_more', 'add_resumo_com_link');


// add a favicon to your 
function blog_favicon() {
	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('wpurl').'/favicon.ico" />';
}
add_action('wp_head', 'blog_favicon');


// add a favicon for your admin
function admin_favicon() {
	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('stylesheet_directory').'/images/favicon.png" />';
}
add_action('admin_head', 'admin_favicon');


// custom admin login logo
function custom_login_logo() {
	echo '<style type="text/css">
	h1 a { background-image: url('.get_bloginfo('template_directory').'/images/custom-login-logo.png) !important; }
	</style>';
}
add_action('login_head', 'custom_login_logo');


// disable all widget areas
function disable_all_widgets($sidebars_widgets) {
	//if (is_home())
		$sidebars_widgets = array(false);
	return $sidebars_widgets;
}
add_filter('sidebars_widgets', 'disable_all_widgets');


// kill the admin nag
if (!current_user_can('edit_users')) {
	add_action('init', create_function('$a', "remove_action('init', 'wp_version_check');"), 2);
	add_filter('pre_option_update_core', create_function('$a', "return null;"));
}


// category id in body and post class
function category_id_class($classes) {
	global $post;
	foreach((get_the_category($post->ID)) as $category)
		$classes [] = 'cat-' . $category->cat_ID . '-id';
		return $classes;
}
add_filter('post_class', 'category_id_class');
add_filter('body_class', 'category_id_class');


// get the first category id
function get_first_category_ID() {
	$category = get_the_category();
	return $category[0]->cat_ID;
}

add_theme_support( 'post-thumbnails' ); 

/** Pagination */
function pagination_funtion() {
// Get total number of pages
global $wp_query;
$total = $wp_query->max_num_pages;
// Only paginate if we have more than one page                   
if ( $total > 1 )  {
    // Get the current page
    if ( !$current_page = get_query_var('paged') )
        $current_page = 1;
                           
        $big = 999999999;
        // Structure of "format" depends on whether we’re using pretty permalinks
        $permalink_structure = get_option('permalink_structure');
        $format = empty( $permalink_structure ) ? '&page=%#%' : 'page/%#%/';
        echo paginate_links(array(
            'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
            'format' => $format,
            'current' => $current_page,
            'total' => $total,
            'mid_size' => 2,
            'type' => 'list'
        ));
    }
}
/** END Pagination */



//add post thumbnails to RSS images
function cwc_rss_post_thumbnail($content) {
		global $post;
		if(has_post_thumbnail($post->ID)) {
				$content = '<p>' . get_the_post_thumbnail($post->ID) .
				'</p>' . get_the_excerpt();
		}

		return $content;
}
add_filter('the_excerpt_rss', 'cwc_rss_post_thumbnail');
add_filter('the_content_feed', 'cwc_rss_post_thumbnail');





/*
 * Custom Post Type and Taxonomy Generation
 * Fornecedores & Categorias/Estados
 */
function register_fornecedores_cpt() {

    $fornecedores_categorias_args = array(
      'labels' => array( 'name' => _x( 'Categorias', 'taxonomy general name' ), 'singular_name' => _x( 'Categoria', 'taxonomy singular name' ) ),
      'show_ui' => true,
      'show_tagcloud' => false,
      'hierarchical' => true,
      'query_var'    => true,
      '_builtin' => false,
      'rewrite' => array(
      	'slug' => 'fornecedores/categoria',
        'with_front' => false
      )
    );
    register_taxonomy( 'fornecedores_categorias', array('guia_de_fornecedores'), $fornecedores_categorias_args );

    $fornecedores_estados_args = array(
      'labels' => array( 'name' => _x( 'Estados', 'taxonomy general name' ), 'singular_name' => _x( 'Estado', 'taxonomy singular name' ) ),
      'show_ui' => true,
      'show_tagcloud' => false,
      'hierarchical' => true,
      'query_var'    => true,
      '_builtin' => false,
      'rewrite' => array(
      	'slug' => 'fornecedores/estado',
        'with_front' => false
      )
    );
    register_taxonomy( 'fornecedores_estados', array('guia_de_fornecedores'), $fornecedores_estados_args );

    $post_type_args = array(
        'public'             => true,
        'labels'             => array(
            'name'               => 'Fornecedores',
            'singular_name'      => 'Fornecedor',
        ),
        //'show_ui'            => true,
        //'query_var'          => true,
        //'publicly_queryable' => true,
        //'has_archive'        => false,
        //'hierarchical'       => true,
        'rewrite'            => array('slug' => 'fornecedores', 'with_front' => true),
        'menu_position'      => 5,
        '_builtin' => false,
        'capability_type'    => 'post',
        'taxonomies'         => array('fornecedores_categorias', 'fornecedores_estados'),
        'supports'           => array('title', 'thumbnail',  'excerpt', 'custom-fields', 'author', 'comments')
    );
    register_post_type( 'guia_de_fornecedores', $post_type_args );
}
add_action( 'init', 'register_fornecedores_cpt' );



//Remove pages from search results
function mySearchFilter($query) {
    $post_type = $_GET['post_type'];
    if ($query->is_search && isset( $post_type ) && locate_template( 'search-' . $post_type . '.php' ) ) {
        $query->set('post_type', array('guia_de_fornecedores'));
    } else if ($query->is_search) {
        $query->set('post_type', 'post');
    }
    return $query;
}



// add_filter('query_vars', 'introduce_qvs');
// function introduce_qvs($qv) {
//    $qv[] = 'categoria';
//    $qv[] = 'estado';
//    return $qv;
// }
// add_filter('pre_get_posts', 'fornecedores_tax_query');



// function fornecedores_tax_query($query) {
   
//    if ($query->is_search) {

//        $tax_query = array();

//        if (!empty($query->query_vars['categoria'])) {
//            $tax_query[] = array(
//                'taxonomy' => 'fornecedores_categorias',
//                'field' => 'id',
//                'terms' => $query->query_vars['categoria']
//            );
//        }

//         if (!empty($query->query_vars['estado'])) {
//            $tax_query[] = array(
//                'taxonomy' => 'fornecedores_estados',
//                'field' => 'id',
//                'terms' => $query->query_vars['estado']
//            );
//         }

//         if (!empty($tax_query)) {
//           $tax_query['relation'] = 'AND'; // you can also use 'OR' here
//           $query->set('tax_query', $tax_query);
//         }

//         //echo "<pre>"; 
//         //print_r($query);

//         return $query;
//    }
// }

// function fb_disable_feed() {
// wp_die( __('No feed available,please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
// }
// add_action('do_feed', 'fb_disable_feed', 1);
// add_action('do_feed_rdf', 'fb_disable_feed', 1);
// add_action('do_feed_rss', 'fb_disable_feed', 1);
// add_action('do_feed_rss2', 'fb_disable_feed', 1);
// add_action('do_feed_atom', 'fb_disable_feed', 1);