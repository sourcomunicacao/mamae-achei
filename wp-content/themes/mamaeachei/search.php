<?php
  // store the post type from the URL string
  $post_type = $_GET['post_type'];
  // check to see if there was a post type in the
  // URL string and if a results template for that
  // post type actually exists
  if ( isset( $post_type ) && locate_template( 'search-' . $post_type . '.php' ) ) {
    // if so, load that template
    get_template_part( 'search', $post_type );
    
    // and then exit out
    exit;
  }
?>
<?php get_header(); ?>
<section class="innerContent">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2>Resultados da busca</h2>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 blogPost">
      <?php 

      $count = 0;

      if ( have_posts() ) : while ( have_posts() ) : the_post(); $count++; ?>
      <?php global $wp_query; $total_results = $wp_query->found_posts; ?>
      
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      
      <h2 class="searchResults">
        <?php the_title(); ?>
      </h2>
      <div class="col-sm-12"> 
        <a href="<?php the_permalink() ?>" rel="bookmark" title="Visualizar <?php the_title_attribute(); ?>">
        <?php 
          if ( has_post_thumbnail() ) {
            the_post_thumbnail('full', array('class'=>'img-responsive'));
          } else {
            echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/img/thumbnail.jpg" class="img-responsive" />';
          }
        ?>
        </a> 
      </div>
      <div class="col-sm-12">
        <a href="<?php the_permalink() ?>" class="btn btn-primary">Visualizar</a></div>
        <div class="clearfix" style="margin-bottom:30px"></div>
      </div>

      <?php if ($count%3 == 0): ?>
        <div class="clearfix" style="margin-bottom:30px"></div>
      <?php endif ?>

      <?php endwhile; else : ?>
      <p>
        <?php _e( 'Desculpe, não encontramos nada.' ); ?>
      </p>
      <?php endif; ?>
    </div>
  </div>
</section>
<?php get_footer(); ?>
