<?php get_header(); ?>
<section class="innerContent">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2>Eventos</h2>
      </div>
    </div>
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 blogPost">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
       <h2 class="searchResults"><?php the_title(); ?> - <?php the_time('d/m/Y') ?></h2>
       <div class="col-sm-12 imgResize">
          <?php the_content(); ?>
       </div>
      </div>
    <?php endwhile; else : ?>
    <p>
      <?php _e( 'Desculpe, não encontramos nada.' ); ?>
    </p>
    <?php endif; ?>
    <?php get_sidebar('anuncios'); ?>
  </div>
</section>
<?php get_footer(); ?>