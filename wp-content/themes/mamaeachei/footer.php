<footer>
<!--<div class="subscribe visible-xs">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <p>Cadastre-se em nosso site e receba as novidades do Mamãe Achei!</p>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right"> </div>
  </div>
</div>-->
<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 visible-xs">
      <ul class="list-inline text-right">
        <li><a href="https://www.facebook.com/mamaeachei?ref=hl" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social/facebook.png"></a></li>
          <li><a href="https://twitter.com/mamaeachei" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social/twitter.png"></a></li>
          <li><a href="http://www.instagram.com/mamaeachei" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social/instagram.png"></a></li>
          <li><a href="https://plus.google.com/114009251164204946686/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social/google+.png"></a></li>
          <li><a href="https://www.pinterest.com/mamaeachei" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social/pinterest.png"></a></li>
          <li><a href="http://mamaeachei.tumblr.com" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social/tumblr.png"></a></li>
      </ul>
      <?php get_search_form(true); ?>
    </div>
  <div class="container"> <span class="line" style="margin:0 10px"></span>
    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
      <h3>Acesso rápido</h3>
      <ul class="markers">
        <li><a href="<?php get_bloginfo('url'); ?>/" title="Home" >Home</a></li>
        <li><a href="<?php get_bloginfo('url'); ?>/categorias" title="Produtos">Produtos</a></li>
        <li><a href="<?php get_bloginfo('url'); ?>/inspiracao" title="Inspiração">Inspiração</a></li>
        <li><a href="<?php get_bloginfo('url'); ?>/como-funciona" title="Como funciona?">Como funciona?</a></li>
        <li><a href="<?php get_bloginfo('url'); ?>/fornecedores" title="Eventos">Guia de Fornecedores</a></li>
        <li><a href="<?php get_bloginfo('url'); ?>/contato" title="Contato">Contato</a></li>
        <li><a href="<?php get_bloginfo('url'); ?>/blog" title="Blog">Blog</a></li>
      </ul>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
      <h3>Mamãe Achei</h3>
      <ul class="markers">
        <li><a href="<?php get_bloginfo('url'); ?>/news" title="News">News</a></li>
        <li><a href="<?php get_bloginfo('url'); ?>/lista-de-lojas" title="Lojas">Lojas</a></li>
        <li><a href="<?php get_bloginfo('url'); ?>/nossa-historinha" title="Nossa Histórinha">Nossa Histórinha</a></li>
      </ul>
    </div>
    <div class="visible-sm clearfix"></div>
    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
      <h3>Informações</h3>
      <ul class="markers">
        <li><a href="<?php get_bloginfo('url'); ?>/como-funciona" title="Como funciona?">Como funciona?</a></li>
        <li><a href="<?php get_bloginfo('url'); ?>/fornecedores" title="Eventos">Guia de Fornecedores</a></li>
        <li><a href="<?php get_bloginfo('url'); ?>/politicas-de-privacidade-e-seguranca" title="Politicas de Privacidade e Segurança">Politicas de Privacidade e Segurança</a></li>
        <li><a href="<?php get_bloginfo('url'); ?>/termos-de-uso" title="Termos de uso">Termos de uso</a></li>
        <li><a href="<?php get_bloginfo('url'); ?>/contato" title="Contato">Contato</a></li>
        <li><a href="mailto:contato@mamaeachei.com.br" title="Como Anunciar?">Como anunciar?</a></li>
      </ul>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
      <h3>Redes sociais</h3>
      <ul>
        <li><a href="https://www.facebook.com/mamaeachei?ref=hl" target="_blank" title="Facebook">Facebook</a></li>
        <li><a href="http://www.instagram.com/mamaeachei" target="_blank" title="Instagram">Instagram</a></li>
        <li><a href="https://twitter.com/mamaeachei" target="_blank" title="Twitter">Twitter</a></li>
        <li><a href="https://www.pinterest.com/mamaeachei" target="_blank" title="Pinterest">Pinterest</a></li>
      </ul>
    </div>
    <div class="clearfix"></div>
    <span class="line" style="margin:0 10px"></span>
    <div class="text-center">
      <p>Mamãe Achei! - © Copyright 2015 .  Direitos Reservados</p>
      <img src="<?php echo get_template_directory_uri(); ?>/img/logo-sour.png" title="Sour Comunicação" alt="Sour Comunicação"> </div>
  </div>
</footer>
<?php wp_footer(); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.0/jquery.cookie.min.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script>
$(document).ready(function() {
  $('#myCarousel2').carousel({
	   interval: 6000
	});
  $('#myCarousel3').carousel({
     interval: 6000
  });
  $('#myCarousel4').carousel({
     interval: 6000
  });
});
</script>
</body>
</html>