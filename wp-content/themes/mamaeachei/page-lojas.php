<?php
/* Template name: Página de lista de lojas */
	$list = '';
	$args = array(
    'orderby'           => 'name', 
    'order'             => 'ASC',
    'child_of'          => 26,
	); 
	$cats = get_terms( 'category', $args );
	$groups = array( );
	if( $cats && is_array( $cats ) ) {
		foreach( $cats as $cat ) {
			$first_letter = strtoupper( $cat->name[0] );
			$groups[ $first_letter ][] = $cat;
		}
		if( !empty( $groups ) ) {
			
			$num = 1;
			$nums = array (3, 6, 9, 12, 15, 18, 21, 24, 27, 30);

			foreach( $groups as $letter => $cats ) {
				$list .= "\n\t" . '<div class="col-sm-4">';
				$list .= "\n\t" . '<h2>' . apply_filters( 'the_title', $letter ) . '</h2>';
				$list .= "\n\t" . '<ul class="listadelojas">';
				foreach( $cats as $cat ) {
					$url = attribute_escape( get_category_link( $cat->term_id ) );
					$count = intval( $cat->count );
					$name = apply_filters( 'the_title', $cat->name );
					$list .= "\n\t\t" . '<li><a href="' . $url . '">' . $name . '</a> (' . $count . ')</li>';
				}
				$list .= "\n\t" . '</ul>';
				$list .= "\n\t" . '</div>';

				if ( in_array($num, $nums) ) $list .= "\n\t" . '<div class="clearfix"></div>';
				
				$num++;

			}
		}
	} else $list .= "\n\t" . '<p>Desculpe, nenhuma categoria encontrada.</p>';
get_header(); ?>
<section class="innerContent">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2>
          <?php the_title(); ?>
        </h2>
      </div>
    </div>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="sidebar">
      <?php print $list; ?>
      <!--<ul class="listStores"> 
        <?php wp_list_categories('orderby=asc&show_count=0&title_li=0&hide_empty=0&use_desc_for_title=0&child_of=26'); ?>
      </ul>-->
    </div>
    </div>
    <?php endwhile; else : ?>
    <p>
      <?php _e( 'Desculpe, não encontramos nada.' ); ?>
    </p>
    <?php endif; ?>
  </div>
</section>
<?php get_footer(); ?>
