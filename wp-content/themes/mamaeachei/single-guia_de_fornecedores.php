<?php

  //response generation function

  $response = "";

  //function to generate response
  function my_contact_form_generate_response($type, $message){

    global $response;

    if($type == "success") $response = "<div class='success'>{$message}</div>";
    else $response = "<div class='error'>{$message}</div>";

  }

  //response messages
  $not_human       = "Resposta incorreta para o cálculo";
  $missing_content = "Por favor preencha todas as informações solicitadas";
  $email_invalid   = "Endereço de e-mail inválido.";
  $message_unsent  = "A mensagem não foi enviada. Tente novamente.";
  $message_sent    = "Obrigado! Sua mensagem foi enviada com sucesso.";

  //user posted variables
  $nome = $_POST['message_nome'];
  $email = $_POST['message_email'];
  $cidade = $_POST['message_cidade'];
  $convidados = $_POST['message_convidados'];
  $assunto = $_POST['message_assunto'];
  $telefone = $_POST['message_telefone'];
  $local = $_POST['message_local'];
  $dataevento = $_POST['message_dataevento'];
  $mensagem = $_POST['message_text'];
  $human = $_POST['message_human'];

  
  //php mailer variables
  $to = get_field('fornecedor_email'); //get_option('admin_email');
  $subject = "Solicitação de orçamento via Mamãe Achei";
  $headers = "From: $nome <$email>" . "\r\n" .
    'Reply-To: ' . $email . "\r\n";

  $message  = $subject."\r\n";
  $message .= "\r\n";
  $message .= "Nome: ".$nome."\r\n";
  $message .= "E-mail: ".$email."\r\n";
  $message .= "Telefone: ".$telefone."\r\n";
  $message .= "Assunto: ".$assunto."\r\n";
  $message .= "Data do Evento: ".$dataevento."\r\n";
  $message .= "Convidados: ".$convidados."\r\n";
  $message .= "Local: ".$local."\r\n";
  $message .= "Cidade: ".$cidade."\r\n";
  $message .= "Mensagem: ".$mensagem."\r\n";

  if(!$human == 0){
    if($human != 2) my_contact_form_generate_response("error", $not_human); //not human!
    else {

      //validate email
      if(!filter_var($email, FILTER_VALIDATE_EMAIL))
        my_contact_form_generate_response("error", $email_invalid);
      else //email is valid
      {
        //validate presence of name and message
        if(empty($name) || empty($message)){
          my_contact_form_generate_response("error", $missing_content);
        }
        else //ready to go!
        {
          $sent = wp_mail($to, $subject, strip_tags($message), $headers);
          if($sent):
            global $wp_query;
            $post_id = $wp_query->post->ID;
            $post = get_post( $post_id );
            $slug = $post->post_name;
            wp_redirect( the_permalink()."?fornecedor=$slug" );
            exit;
          else:
            my_contact_form_generate_response("error", $message_unsent); //message wasn't sent
          endif;
        }
      }
    }
  }
  else if ($_POST['submitted']) my_contact_form_generate_response("error", $missing_content);
?>
<?php define( 'WP_USE_THEMES', false ); get_header(); ?>
<section class="innerContent">

  <div class="container">
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2><a href="<?php echo get_option('home'); ?>/fornecedores" title="Guia de Fornecedores">Guia de Fornecedores</a></h2>
      </div>
      <h4 class="single_fornecedores_cat">Categoria: <?php the_terms( $post->ID, 'fornecedores_categorias', '', ', ', '' ); ?></h4>
    </div>

    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      <?php if (has_post_thumbnail( $post->ID ) ): ?>
      <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
        <img src="<?php echo $image[0]; ?>" class="img-responsive">
      <?php endif; ?>
    </div>    

    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 fornecedorBox">
      
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        
        <div class"col-md-12">
          <?php the_title("<h2>", "</h2>"); ?>
          <?php the_field('fornecedor_cidade'); ?>, <?php the_terms( '', 'fornecedores_estados', '', '', '' ); ?><br>
          <?php the_terms( '', 'fornecedores_categorias', '<div style="margin-bottom:20px;"><strong>', ', ', '</strong></div>' ); ?>
          
          <?php the_excerpt(); ?>

          <?php
            $posturl = get_permalink();
            $posttitle = str_replace( ' ', '%20', get_the_title());
            $whatsURL = 'whatsapp://send?text='.$posttitle.' - '.$posturl;
          ?>

          <br>

          <div id="boxfbfornecedor">
            
            <div class="boxSocialfornecedor">
              <div class="boxRatings">
                <?php if(function_exists('the_ratings')) { the_ratings(); } ?>
              </div>
              <div class="boxFblike">
                <?php echo do_shortcode('[fblike]'); ?>
              </div>
              <div class="boxWhats">
                <a href="<?php echo $whatsURL; ?>" target="_blank">
                  <img style="border-radius:3px;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSI1MTIiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiB3aWR0aD0iNTEyIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIiB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48ZGVmcyBpZD0iZGVmczEyIi8+PGcgaWQ9Imc1MTI0Ij48cmVjdCBoZWlnaHQ9IjUxMiIgaWQ9InJlY3QyOTg3IiBzdHlsZT0iZmlsbDojNjViYzU0O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lIiB3aWR0aD0iNTEyIiB4PSIwIiB5PSIwIi8+PHBhdGggZD0ibSA0NTYsMjUwLjg1MjY2IGMgMCwxMDcuNjA5MDQgLTg3LjkxMjYsMTk0Ljg0NDIgLTE5Ni4zNjM5NywxOTQuODQ0MiAtMzQuNDMwNjYsMCAtNjYuNzc2NzcsLTguODAxNjggLTk0LjkxOTksLTI0LjI0MTYyIEwgNTYuMDAwMDA1LDQ1NiA5MS40Mzc3NDUsMzUxLjQ1NTg0IEMgNzMuNTU5NzE1LDMyMi4wODg3MiA2My4yNjUwMjUsMjg3LjY1NTIzIDYzLjI2NTAyNSwyNTAuODUxMjQgNjMuMjY1MDI1LDE0My4yMzUxNiAxNTEuMTgwNDksNTYgMjU5LjYzNDYzLDU2IDM2OC4wODc0LDU2LjAwMSA0NTYsMTQzLjIzNjU3IDQ1NiwyNTAuODUyNjYgeiBNIDI1OS42MzYwMyw4Ny4wMzE5NiBjIC05MS4wNDA5MiwwIC0xNjUuMDkzOTY1LDczLjQ5MjQ4IC0xNjUuMDkzOTY1LDE2My44MjA3IDAsMzUuODQwNTYgMTEuNjgzNDY1LDY5LjA0MTYyIDMxLjQ0NjA1NSw5Ni4wNDUyOSBsIC0yMC42MjE3Nyw2MC44MzE1MSA2My40NDI4NSwtMjAuMTY0MDMgYyAyNi4wNzEyNiwxNy4xMTMyMyA1Ny4yOTE5NiwyNy4wOTgwNSA5MC44MjU0MywyNy4wOTgwNSA5MS4wMjk2NSwwIDE2NS4wOTM5NiwtNzMuNDg1NDMgMTY1LjA5Mzk2LC0xNjMuODEyMjQgMCwtOTAuMzI2OCAtNzQuMDYyOTIsLTE2My44MTkyOCAtMTY1LjA5MjU2LC0xNjMuODE5MjggeiBtIDk5LjE1NTI2LDIwOC42ODk3MiBjIC0xLjIwOTg5LC0xLjk4ODc5IC00LjQxODUsLTMuMTg2MDIgLTkuMjI0MjQsLTUuNTcwNiAtNC44MTcwNSwtMi4zODc0IC0yOC40ODk2NCwtMTMuOTQ1NTEgLTMyLjg5NCwtMTUuNTM0MjkgLTQuNDE4NDUsLTEuNTkzMDEgLTcuNjMxMjIsLTIuMzkzMDQgLTEwLjgzODM4LDIuMzg0NTggLTMuMjA0MzIsNC43OTAyOCAtMTIuNDI4NTYsMTUuNTM0MjkgLTE1LjI0MjczLDE4LjcyMDMxIC0yLjgwODUzLDMuMTkxNjYgLTUuNjA4NjMsMy41OTAyNiAtMTAuNDI1NjksMS4yMDAwMyAtNC44MDU3OCwtMi4zODczOSAtMjAuMzIxNzcsLTcuNDI4NCAtMzguNzA4MjYsLTIzLjcwMjE1IC0xNC4zMDc0OSwtMTIuNjU4MTUgLTIzLjk2OTc4LC0yOC4yODU0IC0yNi43NzgzMSwtMzMuMDcxNDcgLTIuODA4NTQsLTQuNzc5MDMgLTAuMjk3MiwtNy4zNjIyIDIuMTA5OTMsLTkuNzM5NzUgMi4xNjYyNiwtMi4xNDc5NiA0LjgxNDIzLC01LjU4MTg2IDcuMjI0MTYsLTguMzYzNjQgMi40MDcxMiwtMi43OTQ0NyAzLjIwNzE1LC00Ljc4MTg0IDQuODA4NjEsLTcuOTY5MjYgMS42MTI3MiwtMy4xODg4NCAwLjgwMDAyLC01Ljk3NDg1IC0wLjM5ODYsLTguMzcwNyAtMS4yMDI4NiwtMi4zODMxNyAtMTAuODMyNzQsLTI1Ljg4OTU1IC0xNC44NDQxNSwtMzUuNDQ5IC00LjAxMTM4LC05LjU1OTQ3IC04LjAxMTUsLTcuOTY2NDYgLTEwLjgyNTY4LC03Ljk2NjQ2IC0yLjgwOTk2LDAgLTYuMDE1NjksLTAuNDAwMDIgLTkuMjI5ODcsLTAuNDAwMDIgLTMuMjA5OTcsMCAtOC40MjcwMywxLjE5ODY0IC0xMi44MzU2Miw1Ljk3MzQ0IC00LjQxMDAxLDQuNzgzMjUgLTE2Ljg0MTM4LDE2LjMzMjkxIC0xNi44NDEzOCwzOS44MzM2NSAwLDIzLjUwNDk3IDE3LjI0Mjc5LDQ2LjIxMTMzIDE5LjY1MjczLDQ5LjM5NTk0IDIuNDA0MzEsMy4xNzc1NiAzMy4yODgzOCw1Mi45NzIxIDgyLjIxODExLDcyLjEwMjI4IDQ4Ljk0ODAyLDE5LjExMzI4IDQ4Ljk0ODAyLDEyLjc0NDA3IDU3Ljc3MzY1LDExLjkzNyA4LjgxNDM3LC0wLjc4NzM1IDI4LjQ2OTkyLC0xMS41NDQwMyAzMi40ODgzMiwtMjIuNzAwNzIgNC4wMDg2LC0xMS4xNDk2NCA0LjAwODYsLTIwLjcxODk2IDIuODExNCwtMjIuNzA5MTcgeiIgaWQ9IldoYXRzQXBwXzJfIiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtcnVsZTpldmVub2RkIi8+PC9nPjwvc3ZnPg==" alt="WhatsApp" height="20" width="20"> WhatsApp
                </a>
              </div>
            </div>

          </div>

          <div class="boxComments">
            <?php comments_template(); ?> 
          </div>

        </div>

        <div class="col-md-12">
          <h2 class="title_trace"><span>Solicitar Orçamento</span></h2>
        </div>

        <div id="respond">
          
          <div class="col-md-12">
            <?php echo $response; ?>
            <?php if ($_GET['fornecedor']): ?>
            <div class='success'>Obrigado! Sua mensagem foi enviada com sucesso.</div>
            <?php endif ?>
          </div>
          
          <?php if(!$_GET['fornecedor']): ?>
          <form action="<?php the_permalink(); ?>" method="post" id="formFornecedor">
            
            <div class="col-md-6">
              <p><label for="message_nome">       <input type="text" class="form-control" placeholder="Digite seu Nome *" name="message_nome" value="<?php echo esc_attr($_POST['message_nome']); ?>"></label></p>
              <p><label for="message_email">      <input type="text" class="form-control" placeholder="Digite seu E-mail *" name="message_email" value="<?php echo esc_attr($_POST['message_email']); ?>"></label></p>
              <p><label for="message_cidade">     <input type="text" class="form-control" placeholder="Digite sua Cidade" name="message_cidade" value="<?php echo esc_attr($_POST['message_cidade']); ?>"></label></p>
              <p><label for="message_convidados"> <input type="text" class="form-control" placeholder="Convidados" name="message_convidados" value="<?php echo esc_attr($_POST['message_convidados']); ?>"></label></p>
            </div>

            <div class="col-md-6">
              <p><label for="message_assunto">    <input type="text" class="form-control" placeholder="Digite o Assunto" name="message_assunto" value="<?php echo esc_attr($_POST['message_assunto']); ?>"></label></p>
              <p><label for="message_telefone">   <input type="text" class="form-control" placeholder="Digite o seu Telefone" name="message_telefone" value="<?php echo esc_attr($_POST['message_telefone']); ?>"></label></p>
              <p><label for="message_local">      <input type="text" class="form-control" placeholder="Local" name="message_local" value="<?php echo esc_attr($_POST['message_local']); ?>"></label></p>
              <p><label for="message_dataevento"> <input type="text" class="form-control" placeholder="Data do Evento" name="message_dataevento" value="<?php echo esc_attr($_POST['message_dataevento']); ?>"></label></p>
            </div>
            
            <div class="col-md-12">
              <p><label for="message_text"><textarea type="text" class="form-control" placeholder="Mensagem *" name="message_text"><?php echo esc_textarea($_POST['message_text']); ?></textarea></label></p>
              <p><label for="message_human">Verificação: <span>*</span><br> <span><input type="text" style="width: 60px;" class="form-control" name="message_human"> + 3 = 5<span> </label></p>
              <input type="hidden" name="submitted" value="1">
              <p><input type="submit" class="form-control" value="Enviar »"></p>
            </div>

          </form>
          <?php endif; ?>

        </div>

        <?php endwhile; else : ?>
          <p><?php _e( 'Desculpe, não encontramos nada.' ); ?></p>
        <?php endif; ?>
    
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top:40px">
      <div class="titleHolder">
        <h2>Fornecedores relacionados</h2>
      </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0; padding-bottom:20px;border-bottom:1px solid #eee; margin-bottom:30px;">
      <?php 
      // get the custom post type's taxonomy terms
      $custom_taxterms = wp_get_object_terms( $post->ID, 'fornecedores_estados', array('fields' => 'ids') );
      // arguments
      $args = array(
      'post_type' => 'guia_de_fornecedores',
      'post_status' => 'publish',
      'posts_per_page' => 3, // you may edit this number
      'orderby' => 'rand',
      'tax_query' => array(
          array(
              'taxonomy' => 'fornecedores_estados',
              'field' => 'id',
              'terms' => $custom_taxterms
          )
      ),
      'post__not_in' => array ($post->ID),
      );
      $related_items = new WP_Query( $args );
      // loop over query
      if ($related_items->have_posts()) : while ( $related_items->have_posts() ) : $related_items->the_post(); ?>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 fornecedorBox">
        <a href="<?php the_permalink() ?>" rel="bookmark" title="Visualizar <?php the_title_attribute(); ?>">
        <?php 
          if ( has_post_thumbnail() ) {
            the_post_thumbnail('full', array('class'=>'img-responsive'));
          } else {
            echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/img/thumbnail.jpg" class="img-responsive" />';
          }
        ?>
        <?php the_title("<h2>", "</h2>"); ?>
        </a>
        <?php the_terms( '', 'fornecedores_estados', '', ', ', '' ); ?>
        <br>
        <?php the_terms( '', 'fornecedores_categorias', '<strong>', ', ', '</strong>' ); ?>
      </div>
      <?php
      endwhile;
      endif;
      // Reset Post Data
      wp_reset_postdata();
      ?>



    </div>



  </div>
</section>
<?php get_footer(); ?>