<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>
    <?php
    if(is_home()){
     echo "";
   }
   elseif(is_404()){
     echo "404 (Página não encontrada) - ";
   }
   elseif(is_search()){
     echo "Resultados da busca para ";
     echo wp_specialchars($s, 1);
     echo " - ";
   }
   else{
    echo wp_title();
    echo " - ";
  }
  echo bloginfo('name');
  ?>
</title>
<link href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" rel="icon">
<link href="<?php bloginfo( 'stylesheet_url' ); ?>" rel="stylesheet" >
<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/css/layout.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/fonts/fonts.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet" type="text/css">
<link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <meta name="verification" content="913e174a80a2af42fa240ad119a9159f" />
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-59663252-1', 'auto');
  ga('send', 'pageview');
  </script>
  <script type="text/javascript"> 
  function recordOutboundLink(link, category, action) { 
    try { 
      var pageTracker=_gat._getTracker("UA-59663252-1"); 
      pageTracker._trackEvent(category, action); 
      setTimeout(null, 100) 
    }catch(err){} 
  } 
  </script>
<!--<script>
var trackOutboundLink = function(url) {
   ga('send', 'event', 'outbound', 'click', url, {'hitCallback':
     function () {
     document.location = url;
     }
   });
}
</script>-->
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js" data-pin-hover="true"></script>
<style>
#mc_embed_signup form {padding:0px}
#mc_embed_signup .mc-field-group {padding-bottom:0;min-height:0px}
</style>

<script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us11.list-manage.com","uuid":"01e14fe37b4a3de79ee723637","lid":"a9d5a3073e"}) })</script>

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <div id="fb-root"></div>
  <script>(function(d, s, id) {

    var js, fjs = d.getElementsByTagName(s)[0];

    if (d.getElementById(id)) return;

    js = d.createElement(s); js.id = id;

    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";

    fjs.parentNode.insertBefore(js, fjs);

  }(document, 'script', 'facebook-jssdk'));</script>
  
<!-- Antigo sistema de Newsletter 
<div class="subscribe hidden-xs" style="height: 55px;">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <p>Cadastre-se em nosso site e receba as novidades do Mamãe Achei!</p>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">
      <div id="mc_embed_signup">
        <form action="//mamaeachei.us11.list-manage.com/subscribe/post?u=01e14fe37b4a3de79ee723637&amp;id=a9d5a3073e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
          <div id="mc_embed_signup_scroll">
            <div class="mc-field-group">
              <label for="mce-EMAIL"></label>
              <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Cadastre seu e-mail">
							<!--<script type="text/javascript" src="https://secure.jotformpro.com/jsform/52025909599971"></script>
            </div>
            <div id="mce-responses" class="clear">
              <div class="response" id="mce-error-response" style="display:none"></div>
              <div class="response" id="mce-success-response" style="display:none"></div>
            </div>   
            <div style="position: absolute; left: -5000px;">
             
            	<input type="text" name="b_01e14fe37b4a3de79ee723637_a9d5a3073e" tabindex="-1" value="">
            </div>
            <div class="clear">
             <input type="submit" value="Cadastrar" name="subscribe" id="mc-embedded-subscribe" class="button">
            </div>
          </div>
        </form>
      </div>
      
      <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
      <script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script></div></div></div>
 Fim Antigo sistema de Newsletter -->
  
  <!-- Novo Newsletter TOPO -->
	<div class="fluid-container">
		<div class="grey-bar">
			<div class="container" style="background: #cdcdcd;">
				<div class="col-sm-5" style="background: #cdcdcd;">
					<h5>Cadastre-se e receba as novidades do Mamãe Achei!</h5>
				</div>
				<div class="col-sm-7">
					<script type="text/javascript">
						//<![CDATA[
						if (typeof newsletter_check !== "function") {
							window.newsletter_check = function (f) {
								var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
								if (!re.test(f.elements["ne"].value)) {
									alert("E-mail incorreto");
									return false;
								}
								for (var i=1; i<20; i++) {
									if (f.elements["np" + i] && f.elements["np" + i].required && f.elements["np" + i].value == "") {
										alert("");
										return false;
									}
								}
								if (f.elements["ny"] && !f.elements["ny"].checked) {
									alert("You must accept the privacy statement");
									return false;
								}
								return true;
							}
						}
						//]]&gt;
					</script>
					<div class="newsletter newsletter-subscription">
						<form class="newsletter-form" action="http://www.mamaeachei.com.br/wp-content/plugins/newsletter/do/subscribe.php" method="POST" onsubmit="return newsletter-check(this)">
							<input type="hidden" name="nr" value="page">
							<div class="email" style="margin-top: 10px;">
								<input type="email" name="ne" size="30" class="newsletter-email" placeholder="Insira seu email..." required>
							</div>
							<div class="newsletter-td-submit submit-form">
								<button type="submit" class="newsletter-submit"><span class="glyphicon glyphicon-chevron-right"></span></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

  <header>

<!-- MODAL NEWSLETTER -->
<script>
  $(document).ready(function() {
    if ($.cookie("no_thanks") == null) {
      $('#modal-newsletter').modal("toggle");
          function show_modal(){
            $('#modal-newsletter').modal();
          }
      window.setTimeout(show_modal, 1000);
      }
    $(".nothanks").click(function() {
      document.cookie = "no_thanks=true; expires=Fri, 31 Dec 9999 23:59:59 UTC";

    });
  });

</script>

<div class="modal fade" id="modal-newsletter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close nothanks" data-dismiss="modal" aria-label="close" aria-hidden="true"></button>
        <script type="text/javascript" src="https://secure.jotformpro.com/jsform/52105072767958"></script>
      </div>
    </div>
  </div>
</div>
<!-- FIM MODAL NEWSLETTER -->
    <div class="container">
      <div class="col-md-offset-3 col-lg-offset-3 col-xs-12 col-sm-8 col-md-6 col-lg-6"><a href="<?php get_bloginfo('url'); ?>/"><img src="<?php echo ot_get_option("logo"); ?>" title="" alt="" class="img-responsive"></a></div>
      <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 hidden-xs">
        <ul class="list-inline text-right social-list-header">
          <li><a href="https://www.facebook.com/mamaeachei?ref=hl" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social/facebook.png"></a></li>
          <li><a href="https://twitter.com/mamaeachei" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social/twitter.png"></a></li>
          <li><a href="http://www.instagram.com/mamaeachei" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social/instagram.png"></a></li>
          <li><a href="https://plus.google.com/114009251164204946686/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social/google+.png"></a></li>
          <li><a href="https://www.pinterest.com/mamaeachei" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social/pinterest.png"></a></li>
          <li><a href="http://mamaeachei.tumblr.com" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/social/tumblr.png"></a></li>
        </ul>
        <?php get_search_form(true); ?>
      </div>
      <div class="clearfix"></div>
      <div class="col-xs-12 visible-xs visible-sm">
        <select onchange="location=this.options[this.selectedIndex].value;">
          <option value="#">MENU</option>
          <option value="<?php get_bloginfo('url'); ?>/index.php">Home</option>
          <option value="<?php get_bloginfo('url'); ?>/categorias">Produtos</option>
          <option value="<?php get_bloginfo('url'); ?>/inspiracao">Inspiração</option>
          <option value="<?php get_bloginfo('url'); ?>/como-funciona">Como Funciona?</option>
          <option value="<?php get_bloginfo('url'); ?>/fornecedores">Guia de Fornecedores</option>
          <option value="<?php get_bloginfo('url'); ?>/contato">Contato</option>
          <option value="<?php get_bloginfo('url'); ?>/blog">Blog</option>
        </select>
      </div>
      <ul class="nav nav-pills nav-justified hidden-xs">
        <li><a href="<?php get_bloginfo('url'); ?>/"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-home.png"><br>
          Home</a></li>
          <li role="presentation" class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-produtos.png"><br>
            Produtos <span class="caret"></span> </a>
            <ul class="dropdown-menu" role="menu">
        <!--<li class="dropdown-submenu"><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninas" tabindex="-1">Meninas</a>
            <ul class="dropdown-menu">
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninas/agasalhos/" tabindex="-1">Agasalhos</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninas/blusas/" tabindex="-1">Blusas</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninas/calcas/" tabindex="-1">Calças</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninas/colete-meninas/" tabindex="-1">Colete</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninas/dias-de-chuva/" tabindex="-1">Dias de chuva</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninas/fantasias/" tabindex="-1">Fantasias</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninas/mae-e-filha/" tabindex="-1">Mãe e Filha</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninas/para-dormir/" tabindex="-1">Para Dormir</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninas/praia/" tabindex="-1">Praia</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninas/saias/" tabindex="-1">Saias</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninas/shorts/" tabindex="-1">Shorts</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninas/vestidos/" tabindex="-1">Vestidos</a></li>
            </ul>
          </li>
          <li class="dropdown-submenu"><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninos" tabindex="-1">Meninos</a>
            <ul class="dropdown-menu">
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninos/agasalhos-meninos-inspiracao/" tabindex="-1">Agasalhos</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninos/bermudas-e-shorts/" tabindex="-1">Bermudas e Shorts</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninos/calcas-meninos/" tabindex="-1">Calças</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninos/camisetas-e-polos/" tabindex="-1">Camisetas e Polos</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninos/colete/" tabindex="-1">Colete</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninos/dias-de-chuva-meninos/" tabindex="-1">Dias de chuva</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninos/fantasias-meninos/" tabindex="-1">Fantasias</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninos/pai-e-filho/" tabindex="-1">Pai e Filho</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninos/para-dormir-meninos/" tabindex="-1">Para Dormir</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/meninos/praia-meninos/" tabindex="-1">Praia</a></li>
            </ul>
          </li>
          <li class="dropdown-submenu"><a href="<?php get_bloginfo('url'); ?>/category/categorias/bebes" tabindex="-1">Bebês</a>
            <ul class="dropdown-menu">
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/bebes/batizado/" tabindex="-1">Batizado</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/bebes/bermudas-e-shorts-bebes/" tabindex="-1">Bermudas e Shorts</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/bebes/blusas-bebes/" tabindex="-1">Blusas</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/bebes/bodies/" tabindex="-1">Bodies</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/bebes/calcas-bebes/" tabindex="-1">Calças</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/bebes/camisetas-e-polos-bebes/" tabindex="-1">Camisetas e Polos</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/bebes/conjuntos/" tabindex="-1">Conjuntos</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/bebes/macaquinhos/" tabindex="-1">Macaquinhos</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/bebes/saias-bebes/" tabindex="-1">Saias</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/bebes/saida-maternidade/" tabindex="-1">Saida Maternidade</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/bebes/praia-bebes/" tabindex="-1">Praia</a></li>
            </ul>
          </li>
          <li class="dropdown-submenu"><a href="<?php get_bloginfo('url'); ?>/category/categorias/esporte" tabindex="-1">Esporte</a>
            <ul class="dropdown-menu">
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/esporte/para-meninas/" tabindex="-1">Para meninas</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/esporte/para-meninos/" tabindex="-1">Para meninos</a></li>
            </ul>
          </li>
          <li class="dropdown-submenu"><a href="<?php get_bloginfo('url'); ?>/category/categorias/calcados" tabindex="-1">Calçados</a>
            <ul class="dropdown-menu">
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/calcados/para-meninas-calcados/" tabindex="-1">Para meninas</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/calcados/para-meninos-calcados/" tabindex="-1">Para meninos</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/calcados/para-bebes/" tabindex="-1">Para bebês</a></li>
            </ul>
          </li>
          <li class="dropdown-submenu"><a href="<?php get_bloginfo('url'); ?>/category/categorias/acessorios" tabindex="-1">Acessórios</a>
            <ul class="dropdown-menu">
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/acessorios/para-meninas-acessorios/" tabindex="-1">Para meninas</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/acessorios/para-meninos-acessorios/" tabindex="-1">Para meninos</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/acessorios/para-bebes-acessorios/" tabindex="-1">Para bebês</a></li>
            </ul>
          </li>
          <li class="dropdown-submenu"><a href="<?php get_bloginfo('url') ?>/category/categorias/enxoval" tabindex="-1">Enxoval</a>
            <ul class="dropdown-menu">
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/enxoval/acessorios-enxoval/" tabindex="-1">Acessórios</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/enxoval/alimentacao/" tabindex="-1">Alimentação</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/enxoval/banho/" tabindex="-1">Banho</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/enxoval/cama/" tabindex="-1">Cama</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/enxoval/decoracao/" tabindex="-1">Decoração</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/enxoval/para-maternidade/" tabindex="-1">Para maternidade</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/enxoval/passeio/" tabindex="-1">Passeio</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/enxoval/moveis/" tabindex="-1">Móveis</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/enxoval/seguranca/" tabindex="-1">Segurança</a></li>
            </ul>
          </li>
          <li class="dropdown-submenu"><a href="<?php get_bloginfo('url') ?>/category/categorias/brinquedos" tabindex="-1">Brinquedos</a>
            <ul class="dropdown-menu">
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/brinquedos/para-meninas-brinquedos/" tabindex="-1">Para meninas</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/brinquedos/para-meninos-brinquedos/" tabindex="-1">Para meninos</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/brinquedos/para-bebes-brinquedos/" tabindex="-1">Para bebês</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/brinquedos/para-banho-e-praia/" tabindex="-1">Para banho e praia</a></li>
            </ul>
          </li>
          <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/coisinhas/">Coisinhas</a></li>
          <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/decor/">Decor</a></li>
          <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/livraria/">Livraria</a></li>
          <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/papelaria/">Papelaria</a></li>
          <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/jogos/">Jogos</a></li>
          <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/utensilios/">Utensílios</a></li>
          <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/festas/">Festas</a></li>
          <li class="dropdown-submenu"><a href="<?php get_bloginfo('url') ?>/category/categorias/servicos" tabindex="-1">Serviços</a>
            <ul class="dropdown-menu">
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/servicos/buffets/" tabindex="-1">Buffets</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/servicos/cursos/" tabindex="-1">Cursos</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/servicos/educacao/" tabindex="-1">Educação</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/servicos/feiras/" tabindex="-1">Feiras</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/servicos/fotografia-e-filmagem/" tabindex="-1">Fotografia e Filmagem</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/servicos/saude-infantil/" tabindex="-1">Saúde Infantil</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/servicos/outros/" tabindex="-1">Outros</a></li>
            </ul>
          </li>
          <li class="dropdown-submenu"><a href="<?php get_bloginfo('url') ?>/category/categorias/para-gravidas" tabindex="-1">Para grávidas</a>
            <ul class="dropdown-menu">
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/para-gravidas/acessorios-para-gravidas/" tabindex="-1">Acessórios</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/para-gravidas/calcados-para-gravidas/" tabindex="-1">Calçados</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/para-gravidas/lingerie/" tabindex="-1">Lingerie</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/para-gravidas/moda/" tabindex="-1">Moda</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/para-gravidas/outros-para-gravidas/" tabindex="-1">Outros</a></li>
            </ul>
          </li>
          <li class="dropdown-submenu"><a href="<?php get_bloginfo('url') ?>/category/categorias/para-mamaes" tabindex="-1">Para mamães</a>
            <ul class="dropdown-menu">
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/para-mamaes/acessorios-para-mamaes/" tabindex="-1">Acessórios</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/para-mamaes/calcados-para-mamaes/" tabindex="-1">Calçados</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/para-mamaes/lingerie-para-mamaes/" tabindex="-1">Lingerie</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/para-mamaes/mae-e-filha-para-mamaes/" tabindex="-1">Mãe e Filha</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/para-mamaes/mae-e-filha-para-mamaes/" tabindex="-1">Moda</a></li>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias/para-mamaes/outros-para-mamaes/" tabindex="-1">Outros</a></li>
            </ul>
          </li>
          <li><a href="<?php get_bloginfo('url'); ?>/lista-de-lojas/">Por loja</a></li>-->
          <li class="dropdown-submenu"><a href="#">Por preço</a>
            <ul class="dropdown-menu" style="width:200px"><?php wp_nav_menu( array('menu' => 'Preços', 'menu_class' => 'list-unstyled' )); ?></ul>
          </li>
          <li class="dropdown-submenu"><a href="#">Por categoria</a>
            <ul class="dropdown-menu"><?php wp_nav_menu( array('menu' => 'Categorias', 'menu_class' => 'list-unstyled two-columns' )); ?>
              <li><a href="<?php get_bloginfo('url'); ?>/category/categorias" style="background:#efefef;font-weight:bold;text-align:center;margin-top:5px">Ver mais</a></li></ul>
            </li>
            <li class="dropdown-submenu"><a href="#">Por loja</a>
              <ul class="dropdown-menu"><?php wp_nav_menu( array('menu' => 'Lojas', 'menu_class' => 'list-unstyled two-columns' )); ?>
                <li><a href="<?php get_bloginfo('url'); ?>/lista-de-lojas" style="background:#efefef;font-weight:bold;text-align:center;margin-top:5px">Ver mais</a></li></ul>
              </li> 
            </ul>
          </li>
          <li><a href="<?php get_bloginfo('url'); ?>/blog"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-blog.png"><br>Blog</a></li>
          <li><a href="<?php get_bloginfo('url'); ?>/como-funciona"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-como-funciona2.jpg"><br>Como funciona?</a></li>
          <li><a href="<?php get_bloginfo('url'); ?>/inspiracao"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-inspiracao.png"><br>Inspiração</a></li>
          <li><a href="<?php get_bloginfo('url'); ?>/fornecedores"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-eventos2.jpg"><br>Guia de Fornecedores</a></li>
          <li><a href="<?php get_bloginfo('url'); ?>/contato"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-contato.png"><br>Contato</a></li>
                  
                  </ul>
                </div>
              </div>
            </header>
            <meta name="verification" content="913e174a80a2af42fa240ad119a9159f" />