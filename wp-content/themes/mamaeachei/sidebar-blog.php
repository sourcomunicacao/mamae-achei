<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 sidebar">
<!--<h2 class="odd">Categorias</h2>
      <ul> 
        <?php wp_list_categories('orderby=id&show_count=0&title_li=0&hide_empty=0&use_desc_for_title=0&child_of=49'); ?>
  </ul>-->
<ul>
  <li>
    <h2 class="odd" style="margin-top:20px">Destaque</h2>
    
    <div id="myCarousel4" class="carousel slide">
        <div class="carousel-inner">
          
            <?php $count = 1; ?>
            <?php query_posts('category_name=Destaque&showposts=6'); while (have_posts()) : the_post(); ?>
            <div class="item <?php if($count == 1) echo "active"; $count = 2; ?>">
              <div class="boxProduct"> <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
                <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                <img src="<?php echo $image[0]; ?>" title="" alt="" class="img-responsive">
                <h2 style="height:auto!important;overflow: visible!important;">
                  <?php the_title(); ?>
                </h2>
                <h3>
                  <?php the_field('nome-da-loja'); ?>
                </h3>
                <h4>
                <?php if ( in_category( 'orcar' )) { ?>
                  Orçar
                    <?php } else { ?>
                  R$ <?php the_field('preco'); ?>
            <?php } ?>
                </h4>
                </a> <a href="<?php the_field('url-produto') ?>" onClick="recordOutboundLink(this, 'Outbound Links', '<?php the_field('url-produto') ?>'); window.open(this.href); return false;" class="btn btn-primary">Ir à loja</a> </div>
            </div>
            <?php endwhile; ?>

          <a class="left carousel-control" href="#myCarousel4" data-slide="prev"> 
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" style="top:60%;margin-left:-5px;"></span> 
            <span class="sr-only">Anterior</span> 
          </a>
          <a class="right carousel-control" href="#myCarousel4" data-slide="next"> 
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true" style="top:60%;margin-right:-5px;"></span> 
            <span class="sr-only">Próximo</span> </a> 
          </div>

        </div>


  </li>
  <li> <a href="<?php echo ot_get_option("link_anuncio1"); ?>" target="_blank"><img src="<?php echo ot_get_option("anuncio1"); ?>" class="img-responsive"></a></li>
  <li>
    <h2 class="odd" style="margin-top:20px">Facebook</h2>
    <div class="fb-page" data-href="https://www.facebook.com/MamaeAchei" data-width="100%" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
      <div class="fb-xfbml-parse-ignore">
        <blockquote cite="https://www.facebook.com/MamaeAchei"><a href="https://www.facebook.com/MamaeAchei">Mamãe Achei</a></blockquote>
      </div>
    </div>
  </li>
  <li>
    <h2 class="odd" style="margin-top:20px">Instagram</h2>
    <?php echo do_shortcode('[instagram-feed]') ?>
  </li>
  <li> <a href="<?php echo ot_get_option("link_anuncio2"); ?>" target="_blank"><img src="<?php echo ot_get_option("anuncio2"); ?>" class="img-responsive" style="margin-top:20px"></a>
  
  </li>
</ul>
</div>