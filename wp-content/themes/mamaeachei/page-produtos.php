﻿<?php
/* Template name: Página de Produtos */
get_header(); ?>

<section class="innerContent">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2>Produtos</h2>
      </div>
    </div>
    <?php get_sidebar(); ?>
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <h2 class="searchResults">Produtos encontrados em: <strong>
        <?php
		if (is_category( )) {
  		$cat = get_query_var('cat');
  		$yourcat = get_category ($cat);
  		echo ''. $yourcat->slug;
 		}
		?>
        </strong></h2>
      <?php $args = array('cat' => '8',
	  			   'post_type' => 'post',
				   'posts_per_page' => 24,
				   'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1), );
	query_posts($args);
	if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
        <div class="boxProduct categoryBox"> <a href="<?php the_permalink() ?>" rel="bookmark" title="Mais detalhes de <?php the_title_attribute(); ?>">
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
          <img src="<?php echo $image[0]; ?>" title="Mais detalhes de <?php the_title_attribute(); ?>" alt="<?php the_title(); ?>" class="img-responsive">
          <h2>
            <?php the_title(); ?>
          </h2>
          <h3>
            <?php the_field('nome-da-loja'); ?>
          </h3>
          <h4>
          <?php if ( in_category( 'orcar' )) { ?>
          	Orçar
          		<?php } else { ?>
          	R$ <?php the_field('preco'); ?>
		  <?php } ?>
          </h4>
          </a> <a href="<?php the_field('url-produto') ?>" onClick="recordOutboundLink(this, 'Outbound Links', '<?php the_field('url-produto') ?>'); window.open(this.href); return false;" class="btn btn-primary">Ir à loja</a> </div>
      </div>
      <?php endwhile ?>
      <div class="navigation">
 		<div class="numeric-pagination"><?php pagination_funtion(); ?></div>
	  </div>
	<?php wp_reset_query(); ?>
    <?php else: ?>
      <p>
        <?php _e( 'Desculpe, não encontramos nada.' ); ?>
      </p>
      <?php endif; ?>
    </div>
  </div>
</section>
<?php get_footer(); ?>
