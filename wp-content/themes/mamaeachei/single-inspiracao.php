<?php define( 'WP_USE_THEMES', false ); get_header(); ?>

<section class="innerContent">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2>
          <?php the_title(); ?>
        </h2>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <?php if (has_post_thumbnail( $post->ID ) ): ?>
      <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
      <img src="<?php echo $image[0]; ?>" class="img-responsive inspirationImg">
      <?php endif; ?>
    </div>
    <?php //get_sidebar('inspiracao'); ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="row" style="margin-top:40px">
        <?php $orig_post = $post; global $post; $tags = wp_get_post_tags($post->ID); if ($tags) { $tag_ids = array();
      foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
    $args=array(
    'tag__in' => $tag_ids,
    'category__in' => array(8),
    'post__not_in' => array($post->ID),
    'posts_per_page'=>16, 
    'caller_get_posts'=>1
    );
     
    $my_query = new wp_query($args);
 
    while( $my_query->have_posts() ) {
    $my_query->the_post();
    ?>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="boxProduct insp"> <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
            <img src="<?php echo $image[0]; ?>" title="" alt="" class="img-responsive">
            <h2>
              <?php the_title(); ?>
            </h2>
            <h3>
              <?php the_field('nome-da-loja'); ?>
            </h3>
            <h4>R$
              <?php the_field('preco'); ?>
            </h4>
            </a>  <a href="<?php the_field('url-produto') ?>" onClick="recordOutboundLink(this, 'Outbound Links', '<?php the_field('url-produto') ?>'); window.open(this.href); return false;" class="btn btn-primary">Ir à loja</a> </div>
        </div>
        <? }
    } 
    $post = $orig_post;
    wp_reset_query();
    ?>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
