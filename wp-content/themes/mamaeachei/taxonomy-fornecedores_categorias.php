<?php get_header(); ?>

<section class="innerContent">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2><a href="<?php echo get_option('home'); ?>/fornecedores" title="Guia de Fornecedores">Guia de Fornecedores</a></h2>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <h2 class="searchResults">Fornecedores na Categoria <strong><?php $term = $wp_query->queried_object; echo $term->name; ?></strong></h2>
      <?php $estado = get_option( "taxonomy_$term->term_id" ); ?>
      <?php 
        $count = 0;
        $args = array(
          'tax_query' => array(
              array(
                'taxonomy'         => $term->taxonomy,
                'field'            => 'slug',
                'terms'            => $term->slug,
                'operator'         => 'IN'
              ) 
            )
          );
        $query = new WP_Query($args);
      ?>
      <?php if ($query->have_posts() ) : while ($query->have_posts() ) : $query->the_post(); $count++ ?>
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div class="fornecedorBox"> 
          <a href="<?php the_permalink() ?>" rel="bookmark" title="Mais detalhes de <?php the_title_attribute(); ?>">
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
            <img src="<?php echo $image[0]; ?>" title="Mais detalhes de <?php the_title_attribute(); ?>" alt="<?php the_title(); ?>" class="img-responsive">
            <?php the_title("<h2>", "</h2>"); ?>
          </a>
          <?php the_field('fornecedor_cidade'); ?>, <?php the_terms( '', 'fornecedores_estados', '', '', '' ); ?>
          <br>
          <?php the_terms( '', 'fornecedores_categorias', '<strong>', ', ', '</strong>' ); ?>
        </div>
      </div>
      <?php if ($count%3 == 0): ?><div class="clearfix" style="margin-bottom:30px"></div><?php endif ?>
      <?php endwhile; ?>
      <?php else: ?>
        <p>Nenhum Fornecedor Encontrado.</p>
      <?php endif; ?>
      <?php 
      //$cat = get_category( get_query_var( 'cat' ) );
      //$category = $cat->slug;
      //echo do_shortcode('[ajax_load_more category="'.$category.'" offset="16" posts_per_page="8" button_label="Carregando produtos"]');
      ?>
    </div>
  </div>
</section>
<?php get_footer(); ?>
