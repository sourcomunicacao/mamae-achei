<?php get_header(); ?>
<section class="innerContent">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2>Inspiração</h2>
      </div>
    </div>
    <div class="col-sm-12">
    <div class="row">
    <?php 
      $num = 1;
      $nums = array (3, 6, 9, 12, 15, 18, 21, 24, 27, 30);
    ?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="margin-bottom:30px">
      <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
      <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
      <img src="<?php echo $image[0]; ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>" class="img-responsive"> </a></div>
    <?php 
      if ( in_array($num, $nums) ) echo '<div class="clearfix"></div>';
      $num++;
    ?>
    <?php endwhile; else : ?>
    <p>
      <?php _e( 'Desculpe, não encontramos nada.' ); ?>
    </p>
    <?php endif; ?>
    </div>
   
    </div>
     <?php //get_sidebar('anuncios'); ?>
  </div>
</section>
<?php get_footer(); ?>
