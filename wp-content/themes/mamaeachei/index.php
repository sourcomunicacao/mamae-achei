<?php get_header(); ?>

<section class="banner container">
  <div id="myCarousel" class="carousel slide hidden-xs" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"><a href="<?php echo ot_get_option("link_banner_1"); ?>" target="_blank"><img src="<?php echo ot_get_option("banner1"); ?>" alt="Destaque" title="Destaque" class="img-responsive"></a></div>
      <div class="item"><a href="<?php echo ot_get_option("link_banner_2"); ?>" target="_blank"><img src="<?php echo ot_get_option("banner2"); ?>" alt="Destaque" title="Destaque" class="img-responsive"></a></div>
      <div class="item"><a href="<?php echo ot_get_option("link_banner_3"); ?>" target="_blank"><img src="<?php echo ot_get_option("banner3"); ?>" alt="Destaque" title="Destaque" class="img-responsive"></a></div>
      <div class="item"><a href="<?php echo ot_get_option("link_banner_4"); ?>" target="_blank"><img src="<?php echo ot_get_option("banner4"); ?>" alt="Destaque" title="Destaque" class="img-responsive"></a></div>
      <div class="item"><a href="<?php echo ot_get_option("link_banner_5"); ?>" target="_blank"><img src="<?php echo ot_get_option("banner5"); ?>" alt="Destaque" title="Destaque" class="img-responsive"></a></div>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> 
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> 
      <span class="sr-only">Anterior</span> 
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> 
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> 
      <span class="sr-only">Próximo</span> </a> 
    </div>
    <div class="visible-xs">
    <a href="<?php echo ot_get_option("link_banner_mob_1"); ?>"><img src="<?php echo ot_get_option("banner_mob_1"); ?>" alt="Destaque" title="Destaque" class="img-responsive"></a>
    <br><br>
    <a href="<?php echo ot_get_option("link_banner_mob_2"); ?>"><img src="<?php echo ot_get_option("banner_mob_2"); ?>" alt="Destaque" title="Destaque" class="img-responsive"></a>
    <br><br>
    <a href="<?php echo ot_get_option("link_banner_mob_3"); ?>"><img src="<?php echo ot_get_option("banner_mob_3"); ?>" alt="Destaque" title="Destaque" class="img-responsive"></a>
    </div>
</section>
<section class="type hidden-xs">
  <div class="container">
    <div class="col-xs-12">
    <?php 
      $args = array( 'category_name' => 'inspiracao', 'posts_per_page' => 3 );
      $query = new WP_Query( $args );
      if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
    ?>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="margin-bottom:30px">
      <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
      <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
      <img src="<?php echo $image[0]; ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" class="img-responsive"> </a></div>
    <?php endwhile; wp_reset_postdata(); else : ?>
    <p>
      <?php _e( 'Desculpe, não encontramos nada.' ); ?>
    </p>
    <?php endif; ?>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
      <div class="boys">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center"><img src="<?php echo ot_get_option("meninos"); ?>"></div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <h2 class="boy">Conheça as opções para os <big>meninos</big></h2>
          <p>&nbsp;<br>
		  &nbsp;<?php echo ot_get_option("meninos_desc"); ?></p>
          <a href="http://www.mamaeachei.com.br/category/categorias/meninos/" class="btn btn-boys">Veja mais</a></div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
      <div class="girls">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center"><img src="<?php echo ot_get_option("meninas"); ?>"></div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <h2 class="girl">Conheça as opções para as <big>meninas</big></h2>
          <p>&nbsp;<br>
		  &nbsp;<?php echo ot_get_option("meninas_desc"); ?></p>
          <a href="http://www.mamaeachei.com.br/category/categorias/meninas/" class="btn btn-girls">Veja mais</a></div>
      </div>
    </div>
  </div>
</section>

<section class="stores">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <span class="line"></span>
      <h2>Chegou Agora</h2>
    </div>
    <div>
      <div id="myCarousel3" class="carousel slide">
        <div class="carousel-inner">
          
          <div class="item active">
            <?php query_posts('showposts=6&offset=0&cat=-49,-52,-51,-50,-31'); while (have_posts()) : the_post(); ?>
            <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2">
              <div class="boxProduct"> 
              	<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
                	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                	<img src="<?php echo $image[0]; ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" class="img-responsive">
                	<h2><?php the_title(); ?></h2>
                	<h3><?php the_field('nome-da-loja'); ?></h3>
                	<h4><?php if ( in_category( 'orcar' )) { ?>Orçar<?php } else { ?>R$ <?php the_field('preco'); ?><?php } ?></h4>
                </a>
                <a href="<?php the_field('url-produto') ?>" onClick="recordOutboundLink(this, 'Outbound Links', '<?php the_field('url-produto') ?>'); window.open(this.href); return false;" class="btn btn-primary">Ir à loja</a> 
              </div>
            </div>
            <?php endwhile; ?>
          </div>
          
          <div class="item">
            <?php query_posts('showposts=6&offset=6&cat=-49,-52,-51,-50,-31'); while (have_posts()) : the_post(); ?>
            <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2">
              <div class="boxProduct"> 
              	<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
                	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                	<img src="<?php echo $image[0]; ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" class="img-responsive">
                	<h2><?php the_title(); ?></h2>
                	<h3><?php the_field('nome-da-loja'); ?></h3>
                	<h4><?php if ( in_category( 'orcar' )) { ?>Orçar<?php } else { ?>R$ <?php the_field('preco'); ?><?php } ?></h4>
                </a>
                <a href="<?php the_field('url-produto') ?>" onClick="recordOutboundLink(this, 'Outbound Links', '<?php the_field('url-produto') ?>'); window.open(this.href); return false;" class="btn btn-primary">Ir à loja</a> 
              </div>
            </div>
            <?php endwhile; ?>
          </div>

          <div class="item">
	          <?php query_posts('showposts=6&offset=12&cat=-49,-52,-51,-50,-31'); while (have_posts()) : the_post(); ?>
	          <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2">
	            <div class="boxProduct"> 
	            	<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
	              	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
	              	<img src="<?php echo $image[0]; ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" class="img-responsive">
	              	<h2><?php the_title(); ?></h2>
	              	<h3><?php the_field('nome-da-loja'); ?></h3>
	              	<h4><?php if ( in_category( 'orcar' )) { ?>Orçar<?php } else { ?>R$ <?php the_field('preco'); ?><?php } ?></h4>
	              </a>
	              <a href="<?php the_field('url-produto') ?>" onClick="recordOutboundLink(this, 'Outbound Links', '<?php the_field('url-produto') ?>'); window.open(this.href); return false;" class="btn btn-primary">Ir à loja</a> 
	            </div>
	          </div>
	          <?php endwhile; ?>
          </div>

          <a class="left carousel-control" href="#myCarousel3" data-slide="prev"> 
            <span class="glyphicon glyphicon-chevron-left arrow-arrived-now" aria-hidden="true"></span> 
            <span class="sr-only">Anterior</span> 
          </a>
          <a class="right carousel-control" href="#myCarousel3" data-slide="next"> 
            <span class="glyphicon glyphicon-chevron-right arrow-arrived-now" aria-hidden="true"></span> 
            <span class="sr-only">Próximo</span> </a> 
          </div>

        </div>
      </div>
    </div>
  </div>
</section>

<section class="featured">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <span class="line"></span>
      <h2>Destaque</h2>
    </div>
    <div class="productLine">
      <?php query_posts('category_name=Destaque&showposts=20'); while (have_posts()) : the_post(); ?>
      <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
        <div class="boxProduct"> <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
          <img src="<?php echo $image[0]; ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" class="img-responsive">
          <h2>
            <?php the_title(); ?>
          </h2>
          <h3>
            <?php the_field('nome-da-loja'); ?>
          </h3>
          <h4>
          <?php if ( in_category( 'orcar' )) { ?>
          	Orçar
          		<?php } else { ?>
          	R$ <?php the_field('preco'); ?>
		  <?php } ?>
          </h4>
          </a> <a href="<?php the_field('url-produto') ?>" onClick="recordOutboundLink(this, 'Outbound Links', '<?php the_field('url-produto') ?>'); window.open(this.href); return false;" class="btn btn-primary">Ir à loja</a> </div>
      </div>
      <?php endwhile; ?>
    </div>
  </div>
</section>
<section class="stores">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> <span class="line"></span>
      <h2>Lojas</h2>
    </div>
    <div>
      <div id="myCarousel2" class="carousel slide">
        <div class="carousel-inner">
          
          <div class="item active">
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_1"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja1"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_2"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja2"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_3"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja3"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_4"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja4"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_5"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja5"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_6"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja6"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
          </div>
          <div class="item">
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_7"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja7"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_8"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja8"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_9"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja9"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_10"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja10"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_11"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja11"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_12"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja12"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
          </div>
          <div class="item">
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_13"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja13"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_14"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja14"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_15"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja15"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_16"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja16"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_17"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja17"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_18"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja18"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
          </div>
          <div class="item">
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_19"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja19"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_20"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja20"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_21"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja21"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_22"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja22"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_23"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja23"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2"><a href="<?php echo ot_get_option("link_loja_24"); ?>" target="_blank"><img src="<?php echo ot_get_option("loja24"); ?>" title="Loja" alt="Loja" class="img-responsive"></a> </div>
          </div>
          
          <a class="left carousel-control" href="#myCarousel2" data-slide="prev"> 
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> 
            <span class="sr-only">Anterior</span> 
          </a>
          <a class="right carousel-control" href="#myCarousel2" data-slide="next"> 
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> 
            <span class="sr-only">Próximo</span> </a> 
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
