<?php
/* Template name: Página com sidebar */
get_header(); ?>
<section class="innerContent">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2><?php the_title(); ?></h2>
      </div>
    </div>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
      <?php the_content(); ?>
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 sidebar">
      <ul>
        <li><img src="<?php the_field('imagem1') ?>" class="img-responsive"></li>
        <li><img src="<?php the_field('imagem2') ?>" class="img-responsive"></li>
      </ul>
    </div>
    <?php endwhile; else : ?>
    <p>
      <?php _e( 'Desculpe, não encontramos nada.' ); ?>
    </p>
    <?php endif; ?>
  </div>
</section>
<?php get_footer(); ?>