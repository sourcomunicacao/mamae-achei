<?php
/* Template name: Página últimos posts */
get_header(); ?>

<section class="innerContent">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2><?php the_title(); ?></h2>
      </div>
    </div>
    <?php global $post;	$myposts = get_posts('numberposts=12&category=8');	foreach($myposts as $post) :?>
      <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
        <div class="boxProduct"> <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
            <img src="<?php echo $image[0]; ?>" title="" alt="" class="img-responsive">
          <h2><?php the_title(); ?></h2>
          <h3><?php the_field('nome-da-loja'); ?></h3>
          <h4>R$ <?php the_field('preco'); ?></h4>
          </a> <a href="<?php the_field('url-produto'); ?>" class="btn btn-primary" title="+ detalhes" onclick="trackOutboundLink('<?php the_field('url-produto'); ?>'); return false;" target="_blank">Ir à loja</a> </div>
      </div>
    <?php endforeach; ?>
  </div>
</section>
<?php get_footer(); ?>
