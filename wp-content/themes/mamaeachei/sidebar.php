<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 sidebar hidden-xs">
  <h2 class="even">Busca refinada</h2>
  <ul>
    <?php wp_list_categories('orderby=id&show_count=0&title_li=0&hide_empty=0&use_desc_for_title=0&child_of=3'); ?>
  </ul>
  <h2 class="odd">Categorias</h2>
  <ul>
    <?php wp_list_categories('orderby=asc&show_count=0&title_li=0&hide_empty=0&use_desc_for_title=0&child_of=8'); ?>
  </ul>
  <h2 class="even">Lojas</h2>
  <ul>
    <?php wp_list_categories('orderby=asc&show_count=0&title_li=0&hide_empty=0&use_desc_for_title=0&child_of=26'); ?>
  </ul>
</div>
<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 sidebar visible-xs">
<h2>Filtrar produtos:</h2>
<a class="btn btn-primary" role="button" data-toggle="collapse" href="#porPreco" aria-expanded="false" aria-controls="porPreco" style="display:block;margin-bottom:3px"> Por preço </a>
  <ul class="collapse" id="porPreco">
    <?php wp_list_categories('orderby=id&show_count=0&title_li=0&hide_empty=0&use_desc_for_title=0&child_of=3'); ?>
  </ul>
  <a class="btn btn-primary" role="button" data-toggle="collapse" href="#porCat" aria-expanded="false" aria-controls="porCat" style="display:block;margin-bottom:3px"> Por categoria </a>
  <ul class="collapse" id="porCat">
    <?php wp_list_categories('orderby=asc&show_count=0&title_li=0&hide_empty=0&use_desc_for_title=0&child_of=8'); ?>
  </ul>
 <a class="btn btn-primary" role="button" data-toggle="collapse" href="#porLoja" aria-expanded="false" aria-controls="porLoja" style="display:block;margin-bottom:3px"> Por loja </a>
  <ul class="collapse" id="porLoja">
    <?php wp_list_categories('orderby=asc&show_count=0&title_li=0&hide_empty=0&use_desc_for_title=0&child_of=26'); ?>
  </ul>
</div>
