<?php define( 'WP_USE_THEMES', false ); get_header(); ?>
<section class="innerContent">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2>Produtos</h2>
      </div>
    </div>
    <?php get_sidebar(); ?>
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    <?php if (has_post_thumbnail( $post->ID ) ): ?>
    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
      <img src="<?php echo $image[0]; ?>" style="width:100%">
    <?php endif; ?>
    </div>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 productInfo">
      <h2><?php the_title(); ?></h2>
      <h3><?php the_field('nome-da-loja'); ?></h3>
      <h3>Detalhes do produto</h3>
      <p><?php the_content(); ?></p>
      <h2>R$ <?php the_field('preco') ?></h2><br>
       <a href="<?php the_field('url-produto') ?>" onClick="recordOutboundLink(this, 'Outbound Links', '<?php the_field('url-produto') ?>'); window.open(this.href); return false;" class="btn btn-primary">Ir à loja</a>
       <!--<a href="<?php the_field('url-produto') ?>" onclick="trackOutboundLink('<?php the_field('url-produto') ?>'); return false;" class="btn btn-primary">Ir à loja</a>-->
      <?php endwhile; else : ?>
       <p><?php _e( 'Desculpe, não encontramos nada.' ); ?></p>
      <?php endif; ?>
      <h3>Compartilhe!</h3>
      <?php echo do_shortcode ('[shareaholic app="share_buttons" id="17646051"]'); ?>
      
    </div>
    <div class="clearfix"></div>
    <h2 class="searchResults" style="margin-top:30px">Produtos sugeridos</h2>
    <?php $orig_post = $post; global $post; $tags = wp_get_post_tags($post->ID); if ($tags) { $tag_ids = array();
      foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
    $args=array(
    'tag__in' => $tag_ids,
    'category__not_in' => array(31),
    'post__not_in' => array($post->ID),
    'posts_per_page'=>8, 
    'caller_get_posts'=>1
    );
     
    $my_query = new wp_query($args);
 
    while( $my_query->have_posts() ) {
    $my_query->the_post();
    ?>
     
    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
        <div class="boxProduct related"> <a href="<?php the_permalink() ?>" rel="bookmark" title="Ir para <?php the_title_attribute(); ?>">
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
            <img src="<?php echo $image[0]; ?>" title="" alt="" class="img-responsive">
          <h2><?php the_title(); ?></h2>
          <h3><?php the_field('nome-da-loja'); ?></h3>
         <h4>
          <?php if ( in_category( 'orcar' )) { ?>
          	Orçar
          		<?php } else { ?>
          	R$ <?php the_field('preco'); ?>
		  <?php } ?>
          </h4>
          </a> <a href="<?php the_field('url-produto') ?>" onClick="recordOutboundLink(this, 'Outbound Links', '<?php the_field('url-produto') ?>'); window.open(this.href); return false;" class="btn btn-primary">Ir à loja</a> </div>
      </div>
     
    <? }
    } 
    $post = $orig_post;
    wp_reset_query();
    ?>
    </div>
  </div>
</section>
<?php get_footer(); ?>