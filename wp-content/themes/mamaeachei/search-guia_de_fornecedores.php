<?php get_header(); ?>
<section class="innerContent">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2><a href="<?php echo get_option('home'); ?>/fornecedores" title="Guia de Fornecedores">Guia de Fornecedores</a> - Pesquisa por "<?php echo esc_html( get_search_query( false ) ); ?>"</h2>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0">
      <?php 
      
        $count = 0;
        $s = get_search_query();
        $tax_query = array();

        if ( !empty($_GET['categoria']) && $_GET['categoria'] !== "0" ) {
            $tax_query[] = array(
                'taxonomy' => 'fornecedores_categorias',
                'field' => 'id',
                'terms' => $_GET['categoria']
            );
        }

        if (!empty($_GET['estado']) && $_GET['estado'] !== "0" ) {
          $tax_query[] = array(
              'taxonomy' => 'fornecedores_estados',
              'field' => 'id',
              'terms' => $_GET['estado']
          );
        }

        if (!empty($tax_query)):
          $tax_query['relation'] = 'AND';
          $args = array('s' => $s, 'post_type' => 'guia_de_fornecedores', 'tax_query' => $tax_query);
        else:
          $args = array('s' => $s, 'post_type' => 'guia_de_fornecedores');
        endif;

        $query = new WP_Query( $args );
        
        if ( $query->have_posts() ) : while ( $query->have_posts() ): $query->the_post(); $count++;
      ?>
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 fornecedorBox">
        <a href="<?php the_permalink() ?>" rel="bookmark" title="Visualizar <?php the_title_attribute(); ?>">
        <div>
          <?php 
            if ( has_post_thumbnail() ) {
              the_post_thumbnail('full', array('class'=>'img-responsive'));
            } else {
              echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/img/thumbnail.jpg" class="img-responsive" />';
            }
          ?>
        </div>
        <?php the_title("<h2>", "</h2>"); ?>
        </a>
        <?php the_terms( '', 'fornecedores_estados', '', ', ', '' ); ?>
        <br>
        <?php the_terms( '', 'fornecedores_categorias', '<strong>', ', ', '</strong>' ); ?>
      </div>
      <?php if ($count%3 == 0): ?><div class="clearfix" style="margin-bottom:30px"></div><?php endif ?>
      
      <?php endwhile; else : ?>
        <p><?php _e( 'Desculpe, não encontramos nada.' ); ?></p>
      <?php endif; ?>
  </div>
</section>
<?php get_footer(); ?>
