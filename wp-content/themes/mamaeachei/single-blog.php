<?php get_header(); ?>
<section class="innerContent">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2>Blog</h2>
      </div>
    </div>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 blogPost">
      <h2 class="searchResults"><?php the_title(); ?> - <?php the_time('d/m/Y') ?></h2>
      <div class="col-sm-12">
          <style>
            /*social */
            /*#social-buttons-wrapper { width:100%; height:auto; position:relative; margin:15px 0; z-index:999  }
            #social-buttons-wrapper .social { float:left; width:auto; height:30px; margin-right:15px }
            #social-buttons-wrapper .social div { margin:0 auto }*/
          </style>
          <?php the_content(); ?>
      </div>
      <div class="col-sm-12 boxComments">
        <?php comments_template(); ?> 
      </div>
    </div>
    <?php endwhile; else : ?>
    <p>
      <?php _e( 'Desculpe, não encontramos nada.' ); ?>
    </p>
    <?php endif; ?>
    <?php get_sidebar('blog'); ?>
  </div>
</section>
<?php get_footer(); ?>
