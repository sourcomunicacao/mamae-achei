<?php get_header(); ?>
<section class="innerContent">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2>Eventos</h2>
      </div>
    </div>
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 blogPost">
	<?php if ( have_posts() ) : while (have_posts() ) : the_post(); ?>
      <div class="post" style="overflow:hidden;margin-bottom:30px">
        <h2 class="searchResults">
          <?php the_title(); ?>
          -
          <?php the_time('d/m/Y') ?>
        </h2>
        <div class="col-sm-6"> <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
          <img src="<?php echo $image[0]; ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>" class="img-responsive"> </a> </div>
        <div class="col-sm-6">
          <?php the_excerpt(); ?>
          <a href="<?php the_permalink() ?>" class="btn btn-primary">Leia mais</a></div>
      </div>
      <?php endwhile; ?>
      <div>
      </div>
      <div class="nav-previous alignleft">
        <?php //next_posts_link( 'Próxima página' ); ?>
      </div>
      <div class="nav-next alignright">
        <?php //previous_posts_link( 'Página anterior' ); ?>
      </div>
      <?php else : ?>
      <p>
        <?php _e( 'Desculpe, não encontramos nada.' ); ?>
      </p>
      <?php endif; ?>
    </div>
    <?php get_sidebar('anuncios'); ?>
  </div>
</section>
<?php get_footer(); ?>
