﻿<?php
/* Template name: Página BLOG */
get_header(); ?>

<section class="innerContent">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="titleHolder">
        <h2>Blog</h2>
      </div>
    </div>
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 blogPost">
      <?php $args = array('cat' => '49', 'posts_per_page' => 5, 'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1), );
	query_posts($args);
	while (have_posts()) : the_post(); ?>
      <div class="post" style="overflow:hidden;margin-bottom:30px">
        <h2 class="searchResults">
          <?php the_title(); ?>
          -
          <?php the_time('d/m/Y') ?>
        </h2>
        <div class="col-sm-6"> <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
          <img src="<?php echo $image[0]; ?>" title="" alt="" class="img-responsive"> </a> </div>
        <div class="col-sm-6">
          <?php the_excerpt(); ?>
          <a href="<?php the_permalink() ?>" class="btn btn-primary">Leia mais</a></div>
      </div>
      <?php

endwhile;
?>
      <div class="navigation">
        <div class="alignleft">
          <?php previous_posts_link('&laquo; Página anterior') ?>
        </div>
        <div class="alignright">
          <?php next_posts_link('Próxima página &raquo;') ?>
        </div>
      </div>
      <?php
wp_reset_query();  // Restore global post data
?>
    </div>
    <?php get_sidebar('blog'); ?>
  </div>
</section>
<?php get_footer(); ?>
