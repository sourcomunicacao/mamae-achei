<?php 

if ( ! defined( 'ABSPATH' ) ) exit('You\'re not allowed to see this page'); // Exit if accessed directly

	//css
	add_action('yasr_add_front_script_css', 'yasr_pro_front_script_css' );

		function yasr_pro_front_script_css () {

			//if visitors stats are enabled
	        if (YASR_VISITORS_STATS === 'yes') {
	            wp_enqueue_style( 'jquery-ui','//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css', FALSE, NULL, 'all' );
	            wp_enqueue_style( 'dashicons' ); //dashicons
	        }

		}

	//js
	add_action('yasr_add_front_script_js', 'yasr_pro_front_script_js' );

		function yasr_pro_front_script_js () {

			//if visitors stats are enabled
	        if (YASR_VISITORS_STATS === 'yes') {
	            wp_enqueue_script( 'jquery-ui-progressbar' ); //script
	            wp_enqueue_script( 'jquery-ui-tooltip' ); //script
	        }

	    }


	//DElete caches for wp_super_Cache and wp_rocket
	add_action('yasr_action_on_visitor_vote', 'yasr_delete_cache' );
	add_action('yasr_action_on_update_visitor_vote', 'yasr_delete_cache');


	function yasr_delete_cache($post_id) {

		yasr_wp_super_cache_support($post_id);

        yasr_wp_rocket_support($post_id);

	}

?>