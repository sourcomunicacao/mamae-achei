<?php
/**
 * Plugin Name:  Yet Another Stars Rating
 * Plugin URI: http://wordpress.org/plugins/yet-another-stars-rating/
 * Description: Yet Another Stars Rating turn your WordPress into a complete review website.
 * Version: 1.1.6
 * Author: Dario Curvino
 * Author URI: https://yetanotherstarsrating.com/
 * Text Domain: yet-another-stars-rating
 * Domain Path: languages
 * License: GPL2
 */

/*

Copyright 2015 Dario Curvino (email : d.curvino@tiscali.it)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

if ( ! defined( 'ABSPATH' ) ) exit('You\'re not allowed to see this page'); // Exit if accessed directly

    
define('YASR_VERSION_NUM', '1.1.6');

//Plugin relative path
define( "YASR_ABSOLUTE_PATH", dirname(__FILE__) );

//Plugin RELATIVE PATH without slashes (just the directory's name)
define( "YASR_RELATIVE_PATH", dirname( plugin_basename(__FILE__) ) );

//Plugin language directory: here I've to use relative path
//because load_plugin_textdomain wants relative and not absolute path
define( "YASR_LANG_DIR", YASR_RELATIVE_PATH . '/languages/' );

//Js directory absolute
define ("YASR_JS_DIR",  plugins_url() .'/'. YASR_RELATIVE_PATH . '/js/');

//CSS directory absolute
define ("YASR_CSS_DIR", plugins_url() .'/'. YASR_RELATIVE_PATH . '/css/');

//IMG directory absolute
define ("YASR_IMG_DIR", plugins_url() .'/'. YASR_RELATIVE_PATH . '/img/');


/****** Getting options ******/

//Get general options
$stored_options = get_option( 'yasr_general_options' );

define ("YASR_AUTO_INSERT_ENABLED", $stored_options['auto_insert_enabled']);

if ( YASR_AUTO_INSERT_ENABLED == 1 ) {

	define ("YASR_AUTO_INSERT_WHAT", $stored_options['auto_insert_what']);
	define ("YASR_AUTO_INSERT_WHERE", $stored_options['auto_insert_where']);
	define ("YASR_AUTO_INSERT_SIZE", $stored_options['auto_insert_size']);
	define ("YASR_AUTO_INSERT_EXCLUDE_PAGES", $stored_options['auto_insert_exclude_pages']);
	define ("YASR_AUTO_INSERT_CUSTOM_POST_ONLY", $stored_options['auto_insert_custom_post_only']);

}

//Avoid undefined index
else {
	define ("YASR_AUTO_INSERT_WHAT", NULL);
	define ("YASR_AUTO_INSERT_WHERE", NULL);
	define ("YASR_AUTO_INSERT_SIZE", NULL);
	define ("YASR_AUTO_INSERT_EXCLUDE_PAGES", NULL);
	define ("YASR_AUTO_INSERT_CUSTOM_POST_ONLY", NULL);

}

define ("YASR_SHOW_OVERALL_IN_LOOP", $stored_options['show_overall_in_loop']);
define ("YASR_SHOW_VISITOR_VOTES_IN_LOOP", $stored_options['show_visitor_votes_in_loop']);
define ("YASR_TEXT_BEFORE_STARS", $stored_options['text_before_stars']);

if ( YASR_TEXT_BEFORE_STARS == 1 ) {

	define ("YASR_TEXT_BEFORE_OVERALL", $stored_options['text_before_overall']);
	define ("YASR_TEXT_BEFORE_VISITOR_RATING", $stored_options['text_before_visitor_rating']);
	define ("YASR_TEXT_AFTER_VISITOR_RATING", $stored_options['text_after_visitor_rating']);
	define ("YASR_CUSTOM_TEXT_USER_VOTED", $stored_options['custom_text_user_voted']);

}

define ("YASR_VISITORS_STATS", $stored_options['visitors_stats']);
define ("YASR_ALLOWED_USER", $stored_options['allowed_user']);
define ("YASR_SNIPPET", $stored_options['snippet']);
define ("YASR_ITEMTYPE", $stored_options['snippet_itemtype']);
define ("YASR_SCHEMA_FORMAT", $stored_options['snippet_format']);
define ("YASR_METABOX_OVERALL_RATING", $stored_options['metabox_overall_rating']);    


// this is the URL our updater / license checker pings. This should be the URL of the site with EDD installed
define( 'YASR_EDD_SL_STORE_URL', 'https://yetanotherstarsrating.com' ); 

//Get stored style options 
$style_options = get_option ('yasr_style_options');

if ($style_options) {

	if(isset($style_options['textarea'])) {

		define ("YASR_CUSTOM_CSS_RULES", $style_options['textarea']);

	}

	else {

		define ("YASR_CUSTOM_CSS_RULES", NULL);

	}

	if(isset($style_options['scheme_color_multiset'])) {

		define ("YASR_SCHEME_COLOR", $style_options['scheme_color_multiset']);

	}

	else {

		define ("YASR_SCHEME_COLOR", NULL);
		
	}


}

else {

	define ("YASR_CUSTOM_CSS_RULES", NULL);
	define ("YASR_SCHEME_COLOR", NULL);


}



//Needed for yasr stylish extension 

if (function_exists('yasr_stylish')) {

	if(isset($style_options['stars_set'])) {

		define("YASR_ST_STARS_DIR", $style_options['stars_set']);

	}

	else {

		define("YASR_ST_STARS_DIR", NULL);

	}


	//Needed for yasr stylish extension 
	if(isset($style_options['custom_stars_set'])) {

		define("YASR_ST_CUSTOM_STARS_DIR", $style_options['custom_stars_set']);

	}

	else {

		define("YASR_ST_CUSTOM_STARS_DIR", NULL);

	}

}

//do_action('yasr_define_style_options', $style_options);

/****** End Getting options ******/



// Include function file 
require (YASR_ABSOLUTE_PATH . '/lib/yasr-functions.php');

require (YASR_ABSOLUTE_PATH . '/lib/yasr-admin-actions.php');

require (YASR_ABSOLUTE_PATH . '/lib/yasr-settings-functions.php');

require (YASR_ABSOLUTE_PATH . '/lib/yasr-db-functions.php');

require (YASR_ABSOLUTE_PATH . '/lib/yasr-ajax-functions.php');

require (YASR_ABSOLUTE_PATH . '/lib/yasr-shortcode-functions.php');

$version_installed = get_option('yasr-version') ;

//If this is a fresh new installation

if (!$version_installed ) {

	yasr_install();

}

global $wpdb;

define ("YASR_VOTES_TABLE", $wpdb->prefix . 'yasr_votes');

define ("YASR_MULTI_SET_NAME_TABLE", $wpdb->prefix . 'yasr_multi_set');

define ("YASR_MULTI_SET_FIELDS_TABLE", $wpdb->prefix . 'yasr_multi_set_fields');

define ("YASR_MULTI_SET_VALUES_TABLE", $wpdb->prefix . 'yasr_multi_values');

define ("YASR_LOG_TABLE", $wpdb->prefix . 'yasr_log');

define ("YASR_LOADER_IMAGE", YASR_IMG_DIR . "/loader.gif");


/****** backward compatibility functions ******/

//Remove july 2016
if ($version_installed && $version_installed < '1.0.5') {

	$multiset_options = get_option('yasr_multiset_options');

	if($multiset_options && $multiset_options['scheme_color'] != '') {

		$style_options['scheme_color_multiset'] = $multiset_options['scheme_color'];

	}

	else {

		$style_options['scheme_color_multiset'] = 'light';

	}

	update_option("yasr_style_options", $style_options);

}

//remove end may 2016
if ($version_installed && $version_installed < '1.0.2') {

	$wpdb->query("ALTER TABLE " . YASR_MULTI_SET_FIELDS_TABLE . " CHANGE field_name field_name VARCHAR( 40 ) 
		CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ;");

}

//Remove end April 2016
if ($version_installed && $version_installed < '0.9.9') {

	$stored_options['snippet_format'] = 'microdata';

	update_option("yasr_general_options", $stored_options);

}


//Remove end March 2016
if ($version_installed && $version_installed < '0.9.7') {

	$stored_options['snippet_itemtype'] = 'Product';

	update_option("yasr_general_options", $stored_options);

}


/****** End backward compatibility functions ******/


if ($version_installed != YASR_VERSION_NUM) {

    update_option('yasr-version', YASR_VERSION_NUM);

}

?>
