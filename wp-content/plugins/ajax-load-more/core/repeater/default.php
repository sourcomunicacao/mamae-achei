<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
        <div class="boxProduct categoryBox"> <a href="<?php the_permalink() ?>" rel="bookmark" title="Mais detalhes de <?php the_title_attribute(); ?>">
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
          <img src="<?php echo $image[0]; ?>" title="Mais detalhes de <?php the_title_attribute(); ?>" alt="<?php the_title(); ?>" class="img-responsive">
          <h2>
            <?php the_title(); ?>
          </h2>
          <h3>
            <?php the_field('nome-da-loja'); ?>
          </h3>
          <h4>
          <?php if ( in_category( 'orcar' )) { ?>
          	Orçar
          		<?php } else { ?>
          	R$ <?php the_field('preco'); ?>
		  <?php } ?>
          </h4>
          </a> <a href="<?php the_field('url-produto') ?>" onClick="recordOutboundLink(this, 'Outbound Links', '<?php the_field('url-produto') ?>'); window.open(this.href); return false;" class="btn btn-primary">Ir à loja</a> </div>
      </div>